﻿using Cms.Common.Definition;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.EntityExtensions
{
    public static class FileExtensions
    {
        public static string GetFileUrl(this FileInfo file)
        {
            return $"{Constants.UploadPath}/{file.Extension}/{file.Id}/{file.Name}.{file.Extension}";
        }
    }
}