﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using Cms.DataAccess.Entities;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Cms.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Page> Pages { get; set; }
        public DbSet<ContentLocation> ContentLocations { get; set; }
        public DbSet<ContentItem> ContentItems { get; set; }
        public DbSet<FileInfo> Files { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tag { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = true;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Page>()
                .HasKey(x => x.Id);

            modelBuilder.Entity<Page>()
                .HasMany(x => x.Locations)
                .WithOptional(x => x.Page)
                .HasForeignKey(x => x.PageId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<Page>()
                .HasOptional(x => x.ParentPage)
                .WithMany(x => x.ChildPages)
                .HasForeignKey(x => x.ParentId);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(x => x.Pages)
                .WithRequired(x => x.Author)
                .HasForeignKey(x => x.AuthorId);

            modelBuilder.Entity<ContentLocation>()
                .HasMany(x => x.Items)
                .WithRequired(x => x.Location)
                .HasForeignKey(x => x.LocationId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<ContentLocation>()
                .Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(60)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_LocationNamePageName", 1) { IsUnique = true })
                );

            modelBuilder.Entity<ContentLocation>()
                .Property(x => x.PageName)
                .IsRequired()
                .HasMaxLength(60)
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_LocationNamePageName", 2) { IsUnique = true })
                );

            modelBuilder.Entity<FileInfo>()
               .HasMany(h => h.Tags)
               .WithMany(w => w.Files);

            modelBuilder.Entity<FileInfo>()
                .HasMany(h => h.Categories)
                .WithMany(w => w.Files);
        }
    }
}
