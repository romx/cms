﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cms.DataAccess.Repository
{
    public interface IRepo<T> where T : class
    {
        IDbSet<T> Set { get; }
        Task<T> Find(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includes);
        IQueryable<T> All();
        IQueryable<T> All(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includes);
        IList<T> GetFilteredList<TOrderField>(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, TOrderField>> order = null,
            bool? orderAsc = true,
            int? skip = null,
            int? take = null,
            params Expression<Func<T, object>>[] includes);
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
    }
}
