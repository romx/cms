﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.Repository
{
        public class Repo<T> : IRepo<T> where T : class, IEntity
    {
        private readonly DbContext _db;
        public IDbSet<T> Set => _db.Set<T>(); 

        public Repo(DbContext db)
        {
            _db = db;
        }
        public async Task<T> Find(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includes)
        {
            var set = Set.AsQueryable();
            set = includes.Aggregate(set, (current, i) => current.Include(i));
            return await set.SingleOrDefaultAsync(query);
        }

        public IQueryable<T> All()
        {
            return Set.AsQueryable();
        }

        public IQueryable<T> All(Expression<Func<T, bool>> query, params Expression<Func<T, object>>[] includes)
        {
            var set = Set.AsQueryable();
            set = includes.Aggregate(set, (current, i) => current.Include(i));
            return set.Where(query);
        }

        public IList<T> GetFilteredList<TOrderField>(
            Expression<Func<T, bool>> filter,
            Expression<Func<T, TOrderField>> order = null,
            bool? orderAsc = true,
            int? skip = null,
            int? take = null,
            params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> dbQuery = includes
                .Aggregate<Expression<Func<T, object>>, IQueryable<T>>(Set, (current, navigationProperty) => current.Include(navigationProperty));

            var query = dbQuery.Where(filter).AsQueryable();

            if (order != null)
            {
                if (orderAsc.HasValue && orderAsc.Value)
                {
                    query = query.OrderBy(order).AsQueryable();
                }
                else
                {
                    query = query.OrderByDescending(order).AsQueryable();
                }
            }
            else
            {
                query = query.OrderBy(o => o.CreatedUtc).AsQueryable();
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }


            return query.ToList();
        }

        public void Add(T entity)
        {
            Set.Add(entity);
        }

        public void Delete(T entity)
        {
            Set.Remove(entity);
        }

        public void Update(T entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}
