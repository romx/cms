using System;
using System.Collections.Generic;
using System.Linq;
using Cms.DataAccess.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Constants = Cms.Common.Definition.Constants;

namespace Cms.DataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Cms.DataAccess.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Cms.DataAccess.ApplicationDbContext";
        }

        protected override void Seed(Cms.DataAccess.ApplicationDbContext context)
        {
           // HomeLocations(context);
           // ProgramsLocations(context);
           // SeedRoles(context);
        }

        private void HomeLocations(ApplicationDbContext db)
        {
            var locations = new List<ContentLocation>
            {
                new ContentLocation
                {
                    Name = Constants.StartPage.Slider,
                    PageName = "start",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "Slider 1",
                            Description = "Slider 1 description"
                        },
                        new ContentItem
                        {
                            Title = "Slider 2",
                            Description = "Slider 2 description"
                        },
                        new ContentItem
                        {
                            Title = "Slider 3",
                            Description = "Slider 3 description"
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.StartPage.Services,
                    PageName = "start",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "advocacy",
                            Description = "The Texas Supreme Court issues its ruling on the constitutionality of Texas� funding of schools."
                        },
                        new ContentItem
                        {
                            Title = "family engagement",
                            Description = "Becoming engaged with families in your CommYOUnity shows you are pledging your commitment to our students. YOU make the difference!"
                        },
                        new ContentItem
                        {
                            Title = "member programs",
                            Description = "Texas PTA offers member programs that allow children and families to realize their full potential. Join the fun and benefits!"
                        },
                        new ContentItem
                        {
                            Title = "pta support",
                            Description = "We are committed to providing support for local PTAs through training, guidance, programs, and ideas for success."
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.StartPage.ServicesSlider,
                    PageName = "start",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "advocacy",
                            Description = "The Texas Supreme Court issues its ruling on the constitutionality of Texas� funding of schools."
                        },
                        new ContentItem
                        {
                            Title = "family engagement",
                            Description = "Becoming engaged with families in your CommYOUnity shows you are pledging your commitment to our students. YOU make the difference!"
                        },
                        new ContentItem
                        {
                            Title = "member programs",
                            Description = "Texas PTA offers member programs that allow children and families to realize their full potential. Join the fun and benefits!"
                        },
                        new ContentItem
                        {
                            Title = "pta support",
                            Description = "We are committed to providing support for local PTAs through training, guidance, programs, and ideas for success."
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.StartPage.CounterHeader,
                    PageName = "start",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "back the future",
                            Description = "Texas PTA wants to back the future of every child in the state. We believe it should be a reality for all children to reach their potential. With advocacy at our core, we are committed to building education and taking a comprehensive approach to community success"
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.StartPage.Counter,
                    PageName = "start",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "LOCAL PTAS",
                            Description = "2,478"
                        },
                        new ContentItem
                        {
                            Title = "STATEWIDE MEMBERSHIP",
                            Description = "486,752"
                        },
                        new ContentItem
                        {
                            Title = "STUDENTS SERVED",
                            Description = "1.95M"
                        }
                    }
                },
            };

            db.ContentLocations.AddRange(locations);
            db.SaveChanges();
        }
        private void ProgramsLocations(ApplicationDbContext db)
        {
            var locations = new List<ContentLocation>
            {
                new ContentLocation
                {
                    Name = Constants.ProgramsPage.Slider,
                    PageName = "programs",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "Programs"
                      
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.ProgramsPage.Partners,
                    PageName = "programs",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "HEB"
                        },
                        new ContentItem
                        {
                            Title = "Nationwide"
                        },
                        new ContentItem
                        {
                            Title = "PTA"
                        },
                        new ContentItem
                        {
                            Title = "AIM"
                        },
                        new ContentItem
                        {
                            Title = "VolunteerSpot"
                        },
                        new ContentItem
                        {
                            Title = "PT Avenue"
                        }
                    }
                },
                new ContentLocation
                {
                    Name = Constants.ProgramsPage.Grid,
                    PageName = "programs",
                    Items = new List<ContentItem>
                    {
                        new ContentItem
                        {
                            Title = "CAMP JUST IMAGINE"
                        },
                        new ContentItem
                        {
                            Title = "KIDS FIRST"
                        },
                        new ContentItem
                        {
                            Title = "OUTSTANDING EDUCATOR"
                        },
                        new ContentItem
                        {
                            Title = "REFLECTIONS"
                        },
                        new ContentItem
                        {
                            Title = "SCHOLARSHIPS"
                        },
                        new ContentItem
                        {
                            Title = "CRASH AT YOUR SCHOOL"
                        },
                        new ContentItem
                        {
                            Title = "REQUEST A PROGRAM"
                        }
                    }
                }
            };

            db.ContentLocations.AddRange(locations);
            db.SaveChanges();
        }
        private void SeedRoles(ApplicationDbContext context)
        {
            if (!context.Roles.Any())
            {
                context.Roles.Add(new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "User"
                });

                var adminRole = new IdentityRole
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = "Admin"

                };
                context.Roles.Add(adminRole);

                var hasher = new PasswordHasher();
                var userAdmin = new ApplicationUser
                {
                    Email = "admin@admin.com",
                    PhoneNumber = "123-123-123",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword("admin"),
                    Id = Guid.NewGuid().ToString(),
                    UserName = "admin@admin.com"
                };
                context.Users.Add(userAdmin);


                context.SaveChanges();

                context.Database.ExecuteSqlCommand(
                    $"INSERT INTO AspNetUserRoles (RoleId, UserId) VALUES('{adminRole.Id}','{userAdmin.Id}')");
            }
        }

    }
}
