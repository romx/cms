namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChildPages : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pages", "ParentId", c => c.Int());
            AddColumn("dbo.Pages", "PageStatus", c => c.Int(nullable: false));
            CreateIndex("dbo.Pages", "ParentId");
            AddForeignKey("dbo.Pages", "ParentId", "dbo.Pages", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pages", "ParentId", "dbo.Pages");
            DropIndex("dbo.Pages", new[] { "ParentId" });
            DropColumn("dbo.Pages", "PageStatus");
            DropColumn("dbo.Pages", "ParentId");
        }
    }
}
