namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Uploader : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        CreatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Title);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Extension = c.String(nullable: false),
                        CreatedUtc = c.DateTime(nullable: false),
                        AuthRequired = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 256),
                        CreatedUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Title);
            
            CreateTable(
                "dbo.FileInfoCategories",
                c => new
                    {
                        FileInfo_Id = c.Int(nullable: false),
                        Category_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FileInfo_Id, t.Category_Id })
                .ForeignKey("dbo.Files", t => t.FileInfo_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.FileInfo_Id)
                .Index(t => t.Category_Id);
            
            CreateTable(
                "dbo.FileInfoTags",
                c => new
                    {
                        FileInfo_Id = c.Int(nullable: false),
                        Tag_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.FileInfo_Id, t.Tag_Id })
                .ForeignKey("dbo.Files", t => t.FileInfo_Id, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_Id, cascadeDelete: true)
                .Index(t => t.FileInfo_Id)
                .Index(t => t.Tag_Id);
            
            AddColumn("dbo.ContentItems", "CreatedUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.ContentLocations", "CreatedUtc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Pages", "CreatedUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.Pages", "CreateDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pages", "CreateDate", c => c.DateTime());
            DropForeignKey("dbo.FileInfoTags", "Tag_Id", "dbo.Tags");
            DropForeignKey("dbo.FileInfoTags", "FileInfo_Id", "dbo.Files");
            DropForeignKey("dbo.FileInfoCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.FileInfoCategories", "FileInfo_Id", "dbo.Files");
            DropIndex("dbo.FileInfoTags", new[] { "Tag_Id" });
            DropIndex("dbo.FileInfoTags", new[] { "FileInfo_Id" });
            DropIndex("dbo.FileInfoCategories", new[] { "Category_Id" });
            DropIndex("dbo.FileInfoCategories", new[] { "FileInfo_Id" });
            DropIndex("dbo.Tags", new[] { "Title" });
            DropIndex("dbo.Files", new[] { "Name" });
            DropIndex("dbo.Categories", new[] { "Title" });
            DropColumn("dbo.Pages", "CreatedUtc");
            DropColumn("dbo.ContentLocations", "CreatedUtc");
            DropColumn("dbo.ContentItems", "CreatedUtc");
            DropTable("dbo.FileInfoTags");
            DropTable("dbo.FileInfoCategories");
            DropTable("dbo.Tags");
            DropTable("dbo.Files");
            DropTable("dbo.Categories");
        }
    }
}
