namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContentItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        ImageUrl = c.String(),
                        LinkUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ContentLocations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.ContentLocations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PageName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContentItems", "LocationId", "dbo.ContentLocations");
            DropIndex("dbo.ContentItems", new[] { "LocationId" });
            DropTable("dbo.ContentLocations");
            DropTable("dbo.ContentItems");
        }
    }
}
