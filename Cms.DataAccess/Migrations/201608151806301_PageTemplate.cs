namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PageTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pages", "Template", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pages", "Template");
        }
    }
}
