namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ContentLocationNameUniqueIndex : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ContentLocations", "Name", c => c.String(nullable: false, maxLength: 60));
            AlterColumn("dbo.ContentLocations", "PageName", c => c.String(nullable: false, maxLength: 60));
            CreateIndex("dbo.ContentLocations", new[] { "Name", "PageName" }, unique: true, name: "IX_LocationNamePageName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ContentLocations", "IX_LocationNamePageName");
            AlterColumn("dbo.ContentLocations", "PageName", c => c.String());
            AlterColumn("dbo.ContentLocations", "Name", c => c.String());
        }
    }
}
