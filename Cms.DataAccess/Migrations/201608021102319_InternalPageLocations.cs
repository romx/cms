namespace Cms.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InternalPageLocations : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ContentLocations", "PageId", c => c.Int());
            CreateIndex("dbo.ContentLocations", "PageId");
            AddForeignKey("dbo.ContentLocations", "PageId", "dbo.Pages", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContentLocations", "PageId", "dbo.Pages");
            DropIndex("dbo.ContentLocations", new[] { "PageId" });
            DropColumn("dbo.ContentLocations", "PageId");
        }
    }
}
