﻿using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.UoW
{
    public class UnitOfWorkFactory
    {
        public static UnitOfWork<T> Create<T>() where T : class, IEntity
        {
            return new UnitOfWork<T>(new ApplicationDbContext());
        }
    }

}
