﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.Repository;

namespace Cms.DataAccess.UoW
{
    public class UnitOfWork<T> : IDisposable where T : class, IEntity
    {
        private bool _disposed;
        private readonly DbContext _db;
        private Repo<T> _repository;
        public UnitOfWork(DbContext db)
        {
            _db = db;
        }

        public IRepo<T> Repository => _repository ?? (_repository = new Repo<T>(_db));
        public async Task Save()
        {
            await _db.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed || !disposing) return;
            _db.Dispose();
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
