﻿using System;
using System.Collections.Generic;
using Cms.Common.Definition;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.Entities
{
    public class Page : IEntity
    {
        public Page()
        {
            Locations = new List<ContentLocation>();
        }
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string AuthorId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        public DateTime? ModDate { get; set; }
        public virtual ApplicationUser Author { get; set; }
        public bool IsDeleted { get; set; }
        public PageStatus PageStatus { get; set; }
        public Template Template { get; set; }
        public virtual ICollection<ContentLocation> Locations { get; set; }
        public virtual Page ParentPage { get; set; }
        public virtual ICollection<Page> ChildPages { get; set; }
        public DateTime CreatedUtc { get; set; }
    }
}
