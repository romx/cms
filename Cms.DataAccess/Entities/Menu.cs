﻿using System.Collections.Generic;
using Cms.Common.Definition;

namespace Cms.DataAccess.Entities
{
    public class Menu //TODO : custom menus
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Page> Pages { get; set; }
        public Template Template { get; set; }
    }
}
