﻿using System;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.Entities
{
    public class ContentItem : IEntity
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string LinkUrl { get; set; }
        public virtual ContentLocation Location { get; set; }
        public DateTime CreatedUtc { get; set; }
    }
}
