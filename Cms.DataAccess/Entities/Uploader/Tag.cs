﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.DataAccess.Entities.Uploader
{
    public class Tag : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Index(IsUnique = false)]
        [StringLength(256)]
        public string Title { get; set; }

        public DateTime CreatedUtc { get; set; }

        public virtual List<FileInfo> Files { get; set; }
    }
}