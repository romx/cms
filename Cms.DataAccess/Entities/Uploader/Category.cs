﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.DataAccess.Entities.Uploader
{
    public class Category : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [StringLength(256)]
        [Index(IsUnique = false)]
        public string Title { get; set; }

        public virtual List<FileInfo> Files { get; set; }

        public DateTime CreatedUtc { get; set; }
    }
}