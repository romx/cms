﻿using System;

namespace Cms.DataAccess.Entities.Uploader
{
    public interface IEntity
    {
        DateTime CreatedUtc { get; set; }
    }
}
