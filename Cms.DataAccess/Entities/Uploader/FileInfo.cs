﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cms.DataAccess.Entities.Uploader
{
    [Table("Files")]
    public class FileInfo : IEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [Index(IsUnique = false)]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        public string Extension { get; set; }

        public DateTime CreatedUtc { get; set; }

        public bool AuthRequired { get; set; }
        

        public virtual List<Category> Categories { get; set; }

        public virtual List<Tag> Tags { get; set; }
    }
}