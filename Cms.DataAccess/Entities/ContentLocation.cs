﻿using System;
using System.Collections.Generic;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.DataAccess.Entities
{
    public class ContentLocation : IEntity
    {
        public int Id { get; set; }
        public int? PageId { get; set; }
        public string Name { get; set; }
        public string PageName { get; set; }
        public virtual Page Page { get; set; }
        public virtual  ICollection<ContentItem> Items { get; set; }
        public DateTime CreatedUtc { get; set; }
    }
}
