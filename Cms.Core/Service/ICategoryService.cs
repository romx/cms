﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.Core.Service
{
    public interface ICategoryService
    {
        Task AddOrUpdate(Category category);
        Task Delete(int id);
        Task Delete(Category category);

        Task<Category> GetCategory(int id);
        List<Category> GetCategories(IEnumerable<int> ids = null);
    }
}