﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cms.Common;
using Cms.DataAccess.Entities;

namespace Cms.Core.Service
{
    public interface IContentItemService
    {
        Task<Result<Dictionary<string, List<ContentItem>>>> GetPageItems(string pageName);
        Task<Result<IEnumerable<ContentItem>>> GetLocationItems(string locationName, string pageName);
        Task<Result> CreateItem(ContentItem item);
        Task<Result> UpdateItem(ContentItem item);
        Task<Result> RemoveItem(int id);
    }
}
