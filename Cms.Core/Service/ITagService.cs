﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.Core.Service
{
    public interface ITagService
    {
        Task AddOrUpdate(Tag tag);
        Task Delete(int id);
        Task Delete(Tag tag);
        Task<Tag> GetTag(int id);
        List<Tag> GetTags(IEnumerable<int> ids = null);
    }
}