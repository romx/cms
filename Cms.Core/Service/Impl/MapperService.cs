﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Uploader.DAL.Entities;
using Uploader.Domain;

namespace Uploader.Services.Implementation
{
    public class MapperService
    {
        public static void RegisterMapping()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Category, Domain.BO.Category>()
                    .ForMember(f => f.Files, n => n.Ignore());
                cfg.CreateMap<Tag, Domain.BO.Tag>()
                    .ForMember(f => f.Files, n => n.Ignore());
                cfg.CreateMap<Domain.BO.Category, Category>();
                cfg.CreateMap<Domain.BO.Tag, Tag>();


                cfg.CreateMap<FileInfo, Domain.BO.FileInfo>();
                cfg.CreateMap<Domain.BO.FileInfo, FileInfo>();
            });
        }

        public static TResult Convert<TSource, TResult>(TSource source)
            where TResult : new()
        {
            var target = new TResult();
            Mapper.Map(source, target);

            return target;
        }

        public static List<TResult> ConvertCollection<TSource, TResult>(IList<TSource> sources)
            where TResult : new()
        {
            return sources.Select(Convert<TSource, TResult>).ToList();
        }
    }
}