﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.UoW;

namespace Cms.Core.Service.Impl
{
    public class FileService : IFileService // TODO: exception handling
    {
        private readonly ICategoryService _categoryService;
        private readonly ITagService _tagService;

        public FileService(ICategoryService categoryService, ITagService tagService)
        {
            _categoryService = categoryService;
            _tagService = tagService;
        }
        public async Task Add(FileInfo file)
        {
            if (file == null) throw new ArgumentNullException(nameof(file));

            var catIds = file.Categories.Select(s=>s.Id);
            var tagIds = file.Tags.Where(w => w.Id != 0).Select(s => s.Id);
            var tagOr = file.Tags;

            try
            {
                file.Categories = _categoryService.GetCategories(catIds).ToList();
                file.Tags = _tagService.GetTags(tagIds).ToList();

                using (var uow = UnitOfWorkFactory.Create<FileInfo>())
                {
                    file.Tags.AddRange(tagOr.Where(w => w.Id == 0));
                    uow.Repository.Add(file);
                    await uow.Save();
                }
            }
            catch (Exception)
            {
                throw;
            }

            file.Id = file.Id;
        }

        public async Task Delete(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<FileInfo>())
                {
                    var file = await uow.Repository.Find(s => s.Id == id);
                    if (file != null)
                    {
                        uow.Repository.Delete(file);
                        await uow.Save();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FileInfo> GetFiles()
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<FileInfo>())
                {
                    return uow.Repository.GetFilteredList(
                        f => true,
                        o => o.Name,
                        true,
                        null,
                        null,
                        i => i.Categories,
                        i => i.Tags
                        ).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<FileInfo> GetFile(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<FileInfo>())
                {
                    return await uow.Repository.Find(x => x.Id == id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task ChangeAuthRequired(int id, bool authRequired)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<FileInfo>())
                {
                    var file = await uow.Repository.Find(x => x.Id == id);
                    if (file != null)
                    {
                        file.AuthRequired = authRequired;
                        uow.Repository.Update(file);
                        await uow.Save();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}