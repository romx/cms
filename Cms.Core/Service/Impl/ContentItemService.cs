﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Cms.Common;
using Cms.Common.Definition;
using Cms.DataAccess.Entities;
using Cms.DataAccess.UoW;

namespace Cms.Core.Service.Impl
{
    public class ContentItemService : IContentItemService
    {
        public async Task<Result<Dictionary<string, List<ContentItem>>>> GetPageItems(string pageName)
        {
            var result = new Result<Dictionary<string, List<ContentItem>>>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<ContentItem>())
                {
                    result.Obj = await uow.Repository
                        .All(x => x.Location.PageName == pageName, x => x.Location)
                        .GroupBy(x => x.Location.Name)
                        .ToDictionaryAsync(
                            x => x.Key, 
                            x => x.Select(i => i)
                                  .ToList());

                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result<IEnumerable<ContentItem>>> GetLocationItems(string locationName, string pageName)
        {
            var result = new Result<IEnumerable<ContentItem>>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<ContentItem>())
                {
                    result.Obj = await uow.Repository
                        .All(x => x.Location.PageName == pageName && x.Location.Name == locationName)
                        .ToListAsync();

                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> CreateItem(ContentItem item)
        {
            var result = new Result();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<ContentItem>())
                {
                    uow.Repository.Add(item);
                    await uow.Save();
                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> UpdateItem(ContentItem item)
        {
            var result = new Result();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<ContentItem>())
                {
                    uow.Repository.Update(item);
                    await uow.Save();

                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> RemoveItem(int id)
        {
            var result = new Result();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<ContentItem>())
                {
                    var item = await uow.Repository.Find(x => x.Id == id);
                    if (item != null)
                    {
                        uow.Repository.Delete(item);
                        await uow.Save();

                        result.Status = ResultStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }
    }
}
