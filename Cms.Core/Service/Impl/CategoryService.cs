﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.UoW;

namespace Cms.Core.Service.Impl
{
    public class CategoryService : ICategoryService // TODO: exception handling
    {
        public async Task AddOrUpdate(Category category)
        {
            if (category == null) throw new ArgumentNullException(nameof(category));

            try
            {
                using (var uow = UnitOfWorkFactory.Create<Category>())
                {
                    var cat = await uow.Repository.Find(s => s.Id == category.Id);
                    if (cat == null)
                    {
                        uow.Repository.Add(category);
                    }
                    else
                    {
                        cat.Title = category.Title;
                        uow.Repository.Update(cat);
                    }

                    await uow.Save();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Category>())
                {
                    var cat = await uow.Repository.Find(s => s.Id == id);
                    if (cat != null)
                    {
                        uow.Repository.Delete(cat);
                        await uow.Save();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(Category category)
        {
            if (category == null) throw new ArgumentNullException(nameof(category));
            await Delete(category.Id);
        }

        public async Task<Category> GetCategory(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Category>())
                {
                    return await uow.Repository.Find(x => x.Id == id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Category> GetCategories(IEnumerable<int> ids = null)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Category>())
                {
                    return ids == null
                ? uow.Repository.GetFilteredList(
                    f => true,
                    o => o.Title
                ).ToList()
                : uow.Repository.GetFilteredList(
                    f => ids.Any(a => a == f.Id),
                    o => o.Id
                ).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}