﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.UoW;

namespace Cms.Core.Service.Impl
{
    public class TagService : ITagService // TODO: exception handling
    {
        public async Task AddOrUpdate(Tag tag)
        {
            if (tag == null) throw new ArgumentNullException(nameof(tag));

            try
            {
                using (var uow = UnitOfWorkFactory.Create<Tag>())
                {
                    var t = await uow.Repository.Find(s => s.Id == tag.Id);
                    if (t == null)
                    {
                        uow.Repository.Add(tag);
                    }
                    else
                    {
                        t.Title = tag.Title;
                        uow.Repository.Update(t);
                    }

                    await uow.Save();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Tag>())
                {
                    var cat = await uow.Repository.Find(s => s.Id == id);
                    if (cat != null)
                    {
                        uow.Repository.Delete(cat);
                        await uow.Save();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(Tag tag)
        {
            if (tag == null) throw new ArgumentNullException(nameof(tag));
            await Delete(tag.Id);
        }

        public async Task<Tag> GetTag(int id)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Tag>())
                {
                    return await uow.Repository.Find(x => x.Id == id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Tag> GetTags(IEnumerable<int> ids = null)
        {
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Tag>())
                {
                    return ids == null
                ? uow.Repository.GetFilteredList(
                    f => true,
                    o => o.Title
                    ).ToList()
                : uow.Repository.GetFilteredList(
                    f => ids.Any(a => a == f.Id),
                    o => o.Id
                    ).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}