﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using Cms.Common;
using Cms.Common.Definition;

namespace Cms.Core.Service.Impl
{
    public class ImageService : IImageService
    {
        public Result SavePhoto(HttpPostedFileBase file, string folder)
        {
            var result = new Result();
            if (file == null) return result;
            try
            {
                Image image = Image.FromStream(file.InputStream);

                CheckPath(folder);
                var name = $"{Guid.NewGuid()}.jpg";
                var path = Path.Combine(folder, name);
                image.Save(path, ImageFormat.Jpeg);
                result.Message = name;
                result.Status = ResultStatus.Success;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        private void CheckPath(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }
    }
}
