﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Common;
using Cms.Common.Definition;
using Cms.DataAccess.Entities;
using Cms.DataAccess.UoW;
using Cms.ViewModels.ContentItem;
using Cms.ViewModels.Mappers;
using Cms.ViewModels.Page;

namespace Cms.Core.Service.Impl
{
    public class PageService : IPageService
    {
        public async Task<Result<IEnumerable<PagePreViewModel>>> GetPages()
        {
            var result = new Result<IEnumerable<PagePreViewModel>>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    var pages = await uow.Repository
                         .All(x => !x.IsDeleted)
                         .Include(x => x.Author)
                         .ToListAsync();
                    result.Obj = pages.Select(PageMapper.MapPreview).ToList();
                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result<PageViewModel>> GetPage(int pageId)
        {
            var result = new Result<PageViewModel>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    var page = await uow.Repository.Find(x => x.Id == pageId, x => x.Author);
                    if (page != null)
                    {
                        result.Obj = PageMapper.Map(page);
                        result.Obj.PageList = await GetPageSelectList(uow, page.Id, page.ParentId);
                        result.Obj.ChildPages = await uow.Repository.All(x => x.ParentId == page.ParentId)
                            .Select(x => new TitleViewModel
                            {
                                Id = x.Id,
                                Title = x.Title
                            }).ToArrayAsync();
                        result.Status = ResultStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> CreatePage(PageViewModel model, string userId)
        {
            var result = new Result();
            try
            {
                var page = PageMapper.Map(model);

                page.AuthorId = userId;
                page.CreatedUtc = DateTime.Now;
                page.Locations.Add(CreatePageBanner(page, model.Banner));

                if (!string.IsNullOrEmpty(model.ParentId))
                {
                    page.ParentId = int.Parse(model.ParentId);
                }

                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    uow.Repository.Add(page);

                    await uow.Save();

                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> UpdatePage(PageViewModel model, string userId)
        {
            var result = new Result();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    var page = await uow.Repository.Find(x => x.Id == model.Id, x => x.Author);

                    if (page != null)
                    {
                        page.Title = model.Title;
                        page.Body = model.Body;
                        page.Description = model.Description;
                        page.ModDate = DateTime.Now;
                        if (model.ParentId != "(without parent)")
                        {
                            page.ParentId = int.Parse(model.ParentId);
                        }
                        else
                        {
                            page.ParentId = null;
                        }

                        var banner = page.Locations.FirstOrDefault()?.Items.First();
                        if (banner != null)
                        {
                            banner.Title = model.Banner.Title;
                            banner.Description = model.Banner.Description;
                            banner.ImageUrl = model.Banner.ImageUrl;
                            banner.LinkUrl = model.Banner.LinkUrl;
                        }
                        uow.Repository.Update(page);
                        await uow.Save();

                        result.Status = ResultStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result> RemovePage(int pageId)
        {
            var result = new Result();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    var page = await uow.Repository.Find(x => x.Id == pageId);
                    if (page != null)
                    {
                        page.IsDeleted = true;
                        uow.Repository.Update(page);
                        await uow.Save();
                        result.Status = ResultStatus.Success;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public Result<IEnumerable<TitleViewModel>> GetTitles()
        {
            var result = new Result<IEnumerable<TitleViewModel>>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    result.Obj = uow.Repository
                        .All(x => !x.IsDeleted)
                        .Select(x => new TitleViewModel
                        {
                            Id = x.Id,
                            Title = x.Title
                        })
                        .ToList();
                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        public async Task<Result<IEnumerable<SelectListItem>>> GetPageList()
        {
            var result = new Result<IEnumerable<SelectListItem>>();
            try
            {
                using (var uow = UnitOfWorkFactory.Create<Page>())
                {
                    result.Obj = await GetPageSelectList(uow, 0);
                    result.Status = ResultStatus.Success;
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }

            return result;
        }

        private async Task<IEnumerable<SelectListItem>> GetPageSelectList(UnitOfWork<Page> uow, int pageId, int? parentId = null)
        {
            var list = new List<SelectListItem>
            {
                new SelectListItem { Text = "(without parent)", Value = null, Selected = !parentId.HasValue }   
            };

            var items = await uow.Repository.All(x => !x.IsDeleted && x.PageStatus == PageStatus.Published && x.Id != pageId)
                        .Select(x => new SelectListItem { Text = x.Title, Value = x.Id.ToString(), Selected = parentId.HasValue && x.Id == parentId })
                        .ToArrayAsync();
            list.AddRange(items);
            return list;
        }
            


        private ContentLocation CreatePageBanner(Page page, ContentItemViewModel model) =>
            new ContentLocation
            {
                Name = "page_banner",
                Page = page,
                PageName = $"{page.Title}",
                Items = new List<ContentItem>
                        {
                            new ContentItem
                            {
                                Title = model.Title,
                                Description = model.Description,
                                ImageUrl = model.ImageUrl,
                                LinkUrl = model.LinkUrl
                            }
                        }
            };
    }
}
