﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Common;
using Cms.ViewModels.Page;

namespace Cms.Core.Service
{
    public interface IPageService
    {
        Task<Result<IEnumerable<PagePreViewModel>>> GetPages();
        Task<Result<PageViewModel>> GetPage(int pageId);
        Task<Result> CreatePage(PageViewModel model, string userId);
        Task<Result> UpdatePage(PageViewModel model, string userId);
        Task<Result> RemovePage(int pageId);
        Result<IEnumerable<TitleViewModel>> GetTitles();
        Task<Result<IEnumerable<SelectListItem>>> GetPageList();
    }
}
