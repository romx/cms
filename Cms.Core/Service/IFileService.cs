﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.Core.Service
{
    public interface IFileService
    {
        Task Add(FileInfo file);
        Task Delete(int id);

        List<FileInfo> GetFiles();

        Task<FileInfo> GetFile(int id);

        Task ChangeAuthRequired(int id, bool authRequired);
    }
}