﻿using Cms.Common;
using System.Web;

namespace Cms.Core.Service
{
    public interface IImageService
    {
        Result SavePhoto(HttpPostedFileBase file, string folder);
    }
}
