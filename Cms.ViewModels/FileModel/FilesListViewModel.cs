﻿using System.Collections.Generic;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.ViewModels.FileModel
{
    public class FilesListViewModel
    {
        public List<Category> Categories { get; set; }

        public List<Tag> Tags { get; set; }

        public List<FileInfo> Files { get; set; }
    }
}