﻿using System.Collections.Generic;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.ViewModels.TagModel
{
    public class TagsListViewModel
    {
        public List<Tag> Tags { get; set; }
    }
}