﻿using System.Collections.Generic;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.ViewModels.UploadModel
{
    public class IndexViewModel
    {
        public List<Category> AllCategories { get; set; }

        public List<Tag> AllTags { get; set; }

        public List<FileInfo> Files { get; set; }
    }
}