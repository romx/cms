﻿using System.ComponentModel.DataAnnotations;

namespace Cms.ViewModels.UploadModel
{
    public class GetFileModel
    {
        [Required]
        public string Format { get; set; }

        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}