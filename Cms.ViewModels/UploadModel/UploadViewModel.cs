﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Cms.Common.Attribute;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.ViewModels.UploadModel
{
    public class UploadViewModel
    {
        public UploadViewModel()
        {
            CategoryIds = new List<int>();
            Tags = new List<string>();
        }

        [Required]
        public string FileName { get; set; }

        [Required]
        //[FileExtensions(Extensions = "pdf,doc,docx")]
        //[RegularExpression(@"^.*(?:.PDF|.pdf|.DOCX|.DOC|.docx|.doc)$", 
            //ErrorMessage = "Incorrect file format (pdf, doc, docx)")]
        [FileTypes("pdf,doc,docx")]
        public HttpPostedFileBase File { get; set; }

        public bool AuthRequired { get; set; }

        public List<int> CategoryIds { get; set; }

        public List<string> Tags { get; set; }


        public List<Tag> AllTags { get; set; }
        public List<Category> AllCategories { get; set; }
    }
}