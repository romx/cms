﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Cms.Common.Definition;
using Cms.ViewModels.ContentItem;

namespace Cms.ViewModels.Page
{
    public class PageViewModel
    {
        public PageViewModel()
        {
            Banner = new ContentItemViewModel();
            PageList = new List<SelectListItem>();
            PageStatus = PageStatus.Draft;
        }
        public int Id { get; set; }
        public string ParentId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        [MaxLength(150)]
        public string Description { get; set; }
        [Required]
        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        public string Body { get; set; }
        public string Author { get; set; }
        public ContentItemViewModel Banner { get; set; }
        public IEnumerable<SelectListItem> PageList { get; set; }
        public Template Template { get; set; }
        public PageStatus PageStatus { get; set; }
        public IEnumerable<TitleViewModel> ChildPages { get; set; }
    }
}
