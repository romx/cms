﻿namespace Cms.ViewModels.Page
{
    public class TitleViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
