﻿using System.Collections.Generic;
using Cms.DataAccess.Entities.Uploader;

namespace Cms.ViewModels.CategoryModel
{
    public class CategoriesListViewModel
    {
        public List<Category> Categories { get; set; }
    }
}