﻿using System.Web;

namespace Cms.ViewModels.ContentItem
{
    public class ContentItemViewModel
    {
        public int Id { get; set; }
        public int LocationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string LinkUrl { get; set; }
    }
}
