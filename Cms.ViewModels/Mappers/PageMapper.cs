﻿using System;
using System.Linq;
using Cms.ViewModels.Page;

namespace Cms.ViewModels.Mappers
{
    public class PageMapper
    {
        public static PageViewModel Map(DataAccess.Entities.Page page)
        {
            var model =  new PageViewModel
            {
                Id = page.Id,
                Title = page.Title,
                Description = page.Description,
                Body = page.Body,
                Author = page.Author.UserName,
                PageStatus = page.PageStatus,
                Template = page.Template
            };
            var banner = page.Locations.FirstOrDefault()?.Items.First();
            if (banner != null)
            {
                model.Banner.Title = banner.Title;
                model.Banner.Description = banner.Description;
                model.Banner.ImageUrl = banner.ImageUrl;
                model.Banner.LinkUrl = banner.LinkUrl;
                model.Banner.Id = banner.Id;
                model.Banner.LocationId = banner.LocationId;
            }

            return model;
        }
            

        public static PagePreViewModel MapPreview(DataAccess.Entities.Page page) =>
            new PagePreViewModel
            {
                Id = page.Id,
                Title = page.Title,
                Description = page.Description,
                Author = page.Author.UserName,
                CreateDate = page.CreatedUtc
            };

        public static DataAccess.Entities.Page Map(PageViewModel model) =>
            new DataAccess.Entities.Page
            {
                Id = model.Id,
                Title = model.Title,
                Body = model.Body,
                Description = model.Description,
                PageStatus = model.PageStatus,
                Template = model.Template
            };
    }
}
