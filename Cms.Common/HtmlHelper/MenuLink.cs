﻿namespace Cms.Common.HtmlHelper
{
    public static class MenuLink
    {
        public static string MenuLinkIsActive(this System.Web.Mvc.HtmlHelper html, string action, string controller)
        {
            var routeData = html.ViewContext.RouteData;

            var routeAction = (string)routeData.Values["action"];
            var routeController = (string)routeData.Values["controller"];
            var returnActive = controller == routeController && action == routeAction;
            return returnActive ? "current active" : "";
        }
    }
}
