﻿using System.Web;
using Newtonsoft.Json;

namespace Cms.Common.Extension
{
    public static class ObjectHelper
    {
        public static IHtmlString ToJson(this System.Web.Mvc.HtmlHelper helper, object obj)
        {
            return helper.Raw(
                 JsonConvert.SerializeObject(
                    obj,
                    Formatting.None,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));
        }
    }
}