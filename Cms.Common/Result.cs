﻿using Cms.Common.Definition;

namespace Cms.Common
{
    public class Result<T> : Result where T : class
    {
        public T Obj { get; set; }
        public Result()
        {
            Status = ResultStatus.Error;
        }
    }

    public class Result
    {
        public ResultStatus Status { get; set; }
        public string Message { get; set; }
        public Result()
        {
            Status = ResultStatus.Error;
        }
    }
}
