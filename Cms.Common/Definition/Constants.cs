﻿namespace Cms.Common.Definition
{
    public class Constants
    {
        public const string UploadPath = "~/uploads";
        public class RoleName
        {
            public const string Admin = "Admin";
            public const string User = "User";
        }
        #region locations
        public class StartPage
        {
            public const string Slider = "start_slider";
            public const string ServicesSlider = "start_services_slider";
            public const string Services = "start_services";
            public const string CounterHeader = "start_counter_header";
            public const string Counter = "start_counter";
        }

        public class ProgramsPage
        {
            public const string Slider = "programs_slider";
            public const string Partners = "programs_partners";
            public const string Grid = "programs_grid";
        }
#endregion
    }
}
