﻿namespace Cms.Common.Definition
{
    public enum ResultStatus
    {
        Error = 0,
        Success
    }

    public enum PageStatus
    {
        Draft,
        Published,
        Revision
    }

    public enum Template
    {
        Reflections // TODO: another template types
    }
}
