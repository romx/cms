using System;
using System.Data.Entity;
using Cms.Controllers;
using Cms.Core.Service;
using Cms.Core.Service.Impl;
using Cms.DataAccess;
using Cms.DataAccess.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;

namespace Cms
{
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>(new HierarchicalLifetimeManager());
            container.RegisterType<AccountController>(new InjectionConstructor());
            container.RegisterType<ManageController>(new InjectionConstructor());

            container.RegisterType<IPageService, PageService>();
            container.RegisterType<IContentItemService, ContentItemService>();
            container.RegisterType<IImageService, ImageService>();
            container.RegisterType<ICategoryService, CategoryService>();
            container.RegisterType<ITagService, TagService>();
            container.RegisterType<IFileService, FileService>();
        }
    }
}
