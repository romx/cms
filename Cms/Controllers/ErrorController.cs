﻿using System.Web.Mvc;

namespace Cms.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Error404()
        {
            HttpContext.Response.StatusCode = 404;
            HttpContext.Response.TrySkipIisCustomErrors = true;
            return View();
        }

        public ActionResult Error500()
        {
            HttpContext.Response.StatusCode = 500;
            HttpContext.Response.TrySkipIisCustomErrors = true;
            return View();
        }
    }
}