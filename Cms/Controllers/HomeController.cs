﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Core.Service;

namespace Cms.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContentItemService _contentService;

        public HomeController(IContentItemService contentService)
        {
            _contentService = contentService;
        }

        public async Task<ActionResult> Index()
        {
            var model = await _contentService.GetPageItems("start");
            return View(model.Obj);
        }

        public async Task<ActionResult> Programs()
        {
            var model = await _contentService.GetPageItems("programs");
            return View(model.Obj);
        }
    }
}