﻿using System.IO;
using System.Web.Mvc;
using Cms.Common.Definition;
using Cms.Core.Service;
using Cms.DataAccess.Entities;

namespace Cms.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly IImageService _imageService;

        protected BaseController(IImageService imageService)
        {
            _imageService = imageService;
        }
        protected ContentItem SavePhoto(ContentItem item)
        {
            if (Request.Files.Count > 0)
            {
                var directory = Path.GetFullPath(Path.Combine(HttpContext.Server.MapPath("~"), "Images"));
                var result = _imageService.SavePhoto(Request.Files[0], directory);
                if (result.Status == ResultStatus.Success) item.ImageUrl = result.Message;
            }

            return item;
        }

        protected string SavePhoto()
        {
            if (Request.Files.Count > 0)
            {
                var directory = Path.GetFullPath(Path.Combine(HttpContext.Server.MapPath("~"), "Images"));
                var result = _imageService.SavePhoto(Request.Files[0], directory);
                if (result.Status == ResultStatus.Success) return result.Message;
            }

            return string.Empty;
        }
    }
}