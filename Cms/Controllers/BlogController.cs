﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Core.Service;

namespace Cms.Controllers
{
    public class BlogController : Controller
    {
        private readonly IPageService _pageService;
        public BlogController(IPageService pageService)
        {
            _pageService = pageService;
        }
        public PartialViewResult Titles()
        {
            var result =  _pageService.GetTitles();
            return PartialView(result.Obj);
        }

        public async Task<ActionResult> Post(int id)
        {
            var result = await _pageService.GetPage(id);
            return View(result.Obj);
        }
    }
}