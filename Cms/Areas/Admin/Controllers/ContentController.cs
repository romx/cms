﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Controllers;
using Cms.Core.Service;
using Cms.DataAccess.Entities;

namespace Cms.Areas.Admin.Controllers
{
    [Authorize]
    public class ContentController : BaseController
    {
        private readonly IContentItemService _contentService;
        
        public ContentController(IContentItemService contentService, IImageService imageService) : base (imageService)
        {
            _contentService = contentService;
        }
        public async Task<ActionResult> Start()
        {
            var model = await _contentService.GetPageItems("start");
            return View(model.Obj);
        }

        [HttpPost]
        public async Task<ActionResult> Start(ContentItem item)
        {
            item = SavePhoto(item);
            await _contentService.UpdateItem(item);
            return RedirectToAction("Start");
        }

        public async Task<ActionResult> Programs()
        {
            var model = await _contentService.GetPageItems("programs");
            return View(model.Obj);
        }

        [HttpPost]
        public async Task<ActionResult> Programs(ContentItem item)
        {
            item = SavePhoto(item);
            await _contentService.UpdateItem(item);
            return RedirectToAction("Programs");
        }
    }
}