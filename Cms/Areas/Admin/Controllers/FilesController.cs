﻿using System.Collections.Generic;
using System.Web.Mvc;
using Cms.Common.Definition;
using Cms.Core.Service;
using Cms.DataAccess.Entities.Uploader;
using Cms.ViewModels.FileModel;

namespace Cms.Areas.Admin.Controllers
{
    [Authorize(Roles = Constants.RoleName.Admin)]
    public class FilesController : Controller
    {
        private readonly IFileService _fileService;
        private readonly ICategoryService _categoryService;
        private readonly ITagService _tagService;

        public FilesController(
            IFileService fileService,
            ICategoryService categoryService,
            ITagService tagService
            )
        {
            _fileService = fileService;
            _categoryService = categoryService;
            _tagService = tagService;
        }

        public ActionResult Index()
        {
            var model = new FilesListViewModel
            {
                Tags = _tagService.GetTags(),
                Categories = _categoryService.GetCategories(),
                Files = _fileService.GetFiles()
            };

            return View(model);
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            _fileService.Delete(id);

            //remove file

            return Json(true);
        }

        [HttpPost]
        public JsonResult ChangeAuth(int id, bool authRequired)
        {
            _fileService.ChangeAuthRequired(id, authRequired);

            return Json(true);
        }
    }
}