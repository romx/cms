﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cms.Common.Definition;
using Cms.Core.Service;
using Cms.DataAccess.Entities.Uploader;
using Cms.DataAccess.EntityExtensions;
using Cms.ViewModels.UploadModel;

namespace Cms.Areas.Admin.Controllers
{
    [Authorize(Roles = Constants.RoleName.Admin)]
    public class UploadController : Controller
    {
        private readonly ITagService _tagService;
        private readonly ICategoryService _categoryService;
        private readonly IFileService _fileService;

        public UploadController(
            ITagService tagService,
            ICategoryService categoryService,
            IFileService fileService)

        {
            _tagService = tagService;
            _categoryService = categoryService;
            _fileService = fileService;
        }

        public ActionResult Index()
        {
            var model = new IndexViewModel
            {
                AllTags = _tagService.GetTags(),
                AllCategories = _categoryService.GetCategories(),
                Files = _fileService.GetFiles()
            };

            return View(model);
        }

        public ActionResult Upload()
        {
            var model = new UploadViewModel
            {
                AllCategories = _categoryService.GetCategories(),
                AllTags = _tagService.GetTags()
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Upload(UploadViewModel request)
        {
            var allCategories = _categoryService.GetCategories();
            var allTags = _tagService.GetTags();

            request.Tags = request.Tags
                .Select(s => s.ToLower())
                .GroupBy(o => o)
                .Select(s => s.First()).ToList();

            var createdUtc = DateTime.UtcNow;

            var tags = request.Tags.Select(tag => (Tag) (allTags.FirstOrDefault(a => a.Title.ToLower() == tag) ?? new Tag
            {
                Title = tag, CreatedUtc = createdUtc
            })).ToList();

            tags = tags
                .GroupBy(o => o.Title)
                .Select(s => s.First()).ToList();

            var ext = Path.GetExtension(request.File.FileName);
            ext = ext?.Replace(".", "");

            if (ModelState.IsValid && !string.IsNullOrEmpty(ext))
            {
                var file = new DataAccess.Entities.Uploader.FileInfo
                {
                    Categories = allCategories.Where(w => request.CategoryIds.Any(a => a == w.Id)).ToList(),
                    Tags = tags,
                    Extension = ext,
                    Name = request.FileName,
                    CreatedUtc = createdUtc,
                    AuthRequired = request.AuthRequired
                };

                await _fileService.Add(file);

                var folderUpload = Server.MapPath(Constants.UploadPath);
                var path = Path.Combine(folderUpload, ext, file.Id.ToString(), $"{file.Name}.{file.Extension}");

                CheckFolderExists(path);

                request.File.SaveAs(path);


                TempData["url"] = file.GetFileUrl();

                return RedirectToAction("Upload");
            }

            request.AllCategories = allCategories;
            request.AllTags = allTags;

            return View(request);
        }

        public async Task<ActionResult> GetFile(GetFileModel request)
        {
            if (ModelState.IsValid)
            {
                var file = await _fileService.GetFile(request.Id);

                if (file != null)
                {
                    if (file.AuthRequired
                        && (!User.Identity.IsAuthenticated
                            || !(User.IsInRole(Constants.RoleName.Admin) || User.IsInRole(Constants.RoleName.User))))
                    {
                        return RedirectToAction("Login", "Account", new { returnurl = file.GetFileUrl().Replace("~", "") });
                    }
                    var folderUpload = Server.MapPath(Constants.UploadPath);
                    var path = Path.Combine(folderUpload, file.Extension, file.Id.ToString(),
                        $"{file.Name}.{file.Extension}");

                    if (System.IO.File.Exists(path))
                    {
                        var mime = MimeMapping.GetMimeMapping(path);

                        return File(path, mime);
                    }
                }
            }

            return HttpNotFound();
        }

        private void CheckFolderExists(string path)
        {
            if (path == null) throw new ArgumentNullException(nameof(path));
            var strings = path.Split('\\').ToList();
            strings.RemoveAt(strings.Count - 1);
            var directoryName = string.Join("\\", strings);
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
        }
    }
}