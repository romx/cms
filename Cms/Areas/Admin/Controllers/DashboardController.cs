﻿using System.Web.Mvc;
using Cms.Controllers;
using Cms.Core.Service;

namespace Cms.Areas.Admin.Controllers
{
    public class DashboardController : BaseController
    {
        public DashboardController(IImageService imageService) : base(imageService)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}