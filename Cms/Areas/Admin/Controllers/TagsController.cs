﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Common;
using Cms.Common.Definition;
using Cms.Core.Service;
using Cms.DataAccess.Entities.Uploader;
using Cms.ViewModels.TagModel;

namespace Cms.Areas.Admin.Controllers
{
    [Authorize(Roles = Constants.RoleName.Admin)]
    public class TagsController : Controller
    {
        private readonly ITagService _tagService;

        public TagsController(ITagService tagService)
        {
            _tagService = tagService;
        }

        public ActionResult Index()
        {
            var model = new TagsListViewModel
            {
                Tags = _tagService.GetTags()
            };


            return View(model);
        }


        [HttpPost]
        public JsonDotNetResult GetList()
        {
            var categories = _tagService.GetTags();

            return new JsonDotNetResult(categories);
        }

        [HttpPost]
        public async Task<JsonDotNetResult> AddUpdate(int? id, string title)
        {
            await _tagService.AddOrUpdate(new Tag
            {
                Id = id ?? 0,
                Title = title
            });


            var categories = _tagService.GetTags();

            return new JsonDotNetResult(categories);
        }

        [HttpPost]
        public async Task<JsonDotNetResult> Delete(int id)
        {
            await _tagService.Delete(id);

            var categories = _tagService.GetTags();

            return new JsonDotNetResult(categories);
        }
    }
}