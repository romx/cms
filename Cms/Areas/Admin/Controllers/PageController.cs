﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Controllers;
using Cms.Core.Service;
using Cms.ViewModels.Page;
using Microsoft.AspNet.Identity;

namespace Cms.Areas.Admin.Controllers
{
    public class PageController : BaseController
    {
        private readonly IPageService _pageService;

        public PageController(IPageService pageService, IImageService imageService) : base(imageService)
        {
            _pageService = pageService;
        }
        public async Task<ActionResult> Index()
        {
            var result = await _pageService.GetPages();
            return View(result.Obj);
        }

        public async Task<ActionResult> Details(int id)
        {
            var result = await _pageService.GetPage(id);
            return View(result.Obj);
        }

        [Authorize]
        public async Task<ActionResult> Create()
        {
            var model = new PageViewModel();
            var pageList = await _pageService.GetPageList();
            model.PageList = pageList.Obj;
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(PageViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Banner.ImageUrl = SavePhoto();
                await _pageService.CreatePage(model, User.Identity.GetUserId());
                return  RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize]
        public async Task<ActionResult> Edit(int id)
        {
            var result = await _pageService.GetPage(id);
            return View(result.Obj);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(int id, PageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = SavePhoto();
                if (!string.IsNullOrEmpty(imageUrl)) model.Banner.ImageUrl = imageUrl;

                await _pageService.UpdatePage(model, User.Identity.GetUserId());
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize]
        public async Task<ActionResult> Delete(int id)
        {
            var result = await _pageService.GetPage(id);
            return View(result.Obj);
        }

        // POST: Page/Delete/5
        [HttpPost]
        public async Task<ActionResult> Delete(int id, FormCollection collection)
        {
            await _pageService.RemovePage(id);
            return RedirectToAction("Index");
        }
    }
}
