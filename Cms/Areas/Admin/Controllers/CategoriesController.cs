﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Cms.Common;
using Cms.Common.Definition;
using Cms.Core.Service;
using Cms.DataAccess.Entities.Uploader;
using Cms.ViewModels.CategoryModel;

namespace Cms.Areas.Admin.Controllers
{
    [Authorize(Roles = Constants.RoleName.Admin)]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            var model = new CategoriesListViewModel {Categories = _categoryService.GetCategories()};


            return View(model);
        }


        [HttpPost]
        public JsonDotNetResult GetList()
        {
            var categories = _categoryService.GetCategories();

            return new JsonDotNetResult(categories);
        }

        [HttpPost]
        public async Task<JsonDotNetResult> AddUpdate(int? id, string title)
        {
            await _categoryService.AddOrUpdate(new Category
            {
                Id = id ?? 0,
                Title = title
            });


            var categories = _categoryService.GetCategories();

            return new JsonDotNetResult(categories);
        }

        [HttpPost]
        public async Task<JsonDotNetResult> Delete(int id)
        {
            await _categoryService.Delete(id);

            var categories = _categoryService.GetCategories();

            return new JsonDotNetResult(categories);
        }
    }
}