﻿FileUpload_Init = function (options) {
    var $ = jQuery;
    masterView = masterView || {};

    $(".select-category").tokenize({
        newElements: false,
        displayDropdownOnFocus: true
    });
    $(".select-tag").tokenize();

    masterView.upload = new (function () {
        var model = this;

        model.file = {
            name: ko.observable(),
            ext: ko.observable()
        };

        model.fileInit = function(file) {
            console.log(file);
            if (file) {
                var names = file.name.split(".");
                var ext = "." + names[names.length - 1];
                names.splice(names.length - 1, 1);
                model.file.name(names.join("."));
                model.file.ext(ext);
            } else {
                model.file.name(null);
                model.file.ext(null);
            }
        };
    })();
};