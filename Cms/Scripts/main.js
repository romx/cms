jQuery(function ($) {
    $(".bxslider").bxSlider({
        controls: false,
        auto: false,
        mode: 'fade'
    });
    var lis = $(".clist-text").closest("li");
    $.each(lis, function (i, n) {
        var img = new Image();
        img.src = $(n).attr("data-image");
    });

    lis.hover(function () {
        var that = $(this);
        var readMoreLink = that.attr("data-href");
        var image = that.attr("data-image");
        lis.removeClass("active");
        that.addClass("active");

        if (!$(".blue-block .half-col:nth-child(1)").is(":visible")) {
            location.href = readMoreLink;
        }

        var title = that.find(".clist-text b").text();
        var text = that.find(".clist-text p").text();

        $(".bbb").text(title);
        $(".bbbt").text(text);
        $(".bbbl").attr("href", readMoreLink);
        $(".clist-item > div").css("background-image", "url(" + image + ")");
    });
    lis.filter(":eq(0)").mouseenter();

    $(".sel-box").niceSelect();

    var selA = $(".tab-titles > a");
    var selCont = $(".tab-contents > div");
    selA.click(function (e) {
        e.preventDefault();
        var th = $(this);
        selA.removeClass("active");
        selCont.removeClass("active");
        th.addClass("active");
        var href = th.attr("href");
        selCont.filter("[data-link='"+href+"']").addClass("active");
    });
});