﻿Admin_Category_Service = new (function () {
    var that = this;
    that.urls = {
        getlist: "",
        add: "",
        del: ""
    };
    that.init = function (options) {
        that.urls.add = options.add || "";
        that.urls.del = options.del || "";
        that.urls.getlist = options.getlist || "";
    };
    that.getList = function() {
        return $.ajax({
            type: "post",
            dataType: "json",
            url: that.urls.getlist
        });
    };
    that.add = function(id, title) {
        return $.ajax({
            type: "post",
            dataType: "json",
            url: that.urls.add,
            data: {
                id: id,
                title: title
            }
        });
    };
    that.del = function(id) {
        return $.ajax({
            type: "post",
            dataType: "json",
            url: that.urls.del,
            data: {
                id: id
            }
        });
    };
})();

Admin_Category = function (options) {
    masterView = masterView || {};

    options.categories = options.categories || [];

    masterView.catModel = new (function() {
        var model = this;

        model.categories = ko.observableArray();
        ko.mapping.fromJS(options.categories, {}, model.categories);

        model.add = {
            title: ko.observable(),
            add: function() {
                if (model.add.title()) {
                    Admin_Category_Service
                        .add(null, model.add.title())
                        .done(function (data) {
                            ko.mapping.fromJS(data, {}, model.categories);
                            model.add.title(null);
                            model.paged.page(1);
                        });
                }
            }
        };

        model.edit = {
            currentId: ko.observable(),
            edit: function(item) {
                model.edit.currentId(item.Id());
            },
            save: function (item) {
                if (item.Title()) {
                    Admin_Category_Service
                        .add(item.Id(), item.Title())
                        .done(function(data) {
                            //model.categories(data);
                            model.edit.currentId(null);
                        });
                }
            }
        }

        model.recordRemove = function (item) {
            if (confirm("Are you sure?")) {
                Admin_Category_Service
                    .del(item.Id())
                    .done(function (data) {
                        ko.mapping.fromJS(data, {}, model.categories);
                    });
            }
        };


        model.paged = new (function () {
            var perPage = 15;
            var that = this;

            that.page = ko.observable(1);
            that.searchForm = {
                query: ko.observable(null)
            };

            that.searchForm.query.subscribe(function (newVal) {
                that.page(1);
            });

            that.pagPrev = function () {
                if (that.page() > 1) {
                    that.page(that.page() - 1);
                }
            };

            that.pagNext = function () {
                if (that.page() < that.totalPage()) {
                    that.page(that.page() + 1);
                }
            };

            that.filtered = ko.computed(function () {
                var arrayFilter = ko.utils.arrayFilter(model.categories(), function (cat) {
                    return !that.searchForm.query() || cat.Title().toLowerCase().indexOf(that.searchForm.query().toLowerCase()) > -1;
                });
                return arrayFilter.slice((that.page() - 1) * perPage, that.page() * perPage);
            });


            that.totalPage = ko.computed(function () {
                var arrayFilter = ko.utils.arrayFilter(model.categories(), function (cat) {
                    return !that.searchForm.query() || cat.Title().toLowerCase().indexOf(that.searchForm.query().toLowerCase()) > -1;
                });
                return Math.ceil(arrayFilter.length / perPage) || 1;
            });
        });


    })();
};