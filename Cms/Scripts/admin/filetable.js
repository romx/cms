﻿Admin_File_Table = function (options) {
    var $ = jQuery;
    masterView = masterView || {};

    masterView.tableFiles = new (function () {
        var model = this;

        model.categories = ko.observableArray(options.categories || []);
        model.tags = ko.observableArray(options.tags || []);

        model.files = ko.observableArray(options.files || []);

        model.paged = new (function () {
            var perPage = 15;
            var that = this;

            that.page = ko.observable(1);
            that.searchForm = {
                query: ko.observable(null),
                categoryId: ko.observable(),
                tagId: ko.observable(),
                clean: function () {
                    that.searchForm.query(null);
                    that.searchForm.categoryId(null);
                    that.searchForm.tagId(null);
                }
            };

            that.searchForm.query.subscribe(function (newVal) {
                that.page(1);
            });

            that.pagPrev = function () {
                if (that.page() > 1) {
                    that.page(that.page() - 1);
                }
            };

            that.pagNext = function () {
                if (that.page() < that.totalPage()) {
                    that.page(that.page() + 1);
                }
            };

            var compare = function (item) {
                return (!that.searchForm.query() ||
                    item.Name.toLowerCase().indexOf(that.searchForm.query().toLowerCase()) > -1) &&
                (!that.searchForm.categoryId() ||
                    item.Categories.find(function (cat) { return cat.Id == that.searchForm.categoryId() })) &&
                (!that.searchForm
                    .tagId() ||
                    item.Tags.find(function (tag) { return tag.Id == that.searchForm.tagId() }));
            };

            that.filtered = ko.computed(function () {
                var arrayFilter = ko.utils.arrayFilter(model.files(), compare);
                return arrayFilter.slice((that.page() - 1) * perPage, that.page() * perPage);
            });


            that.totalPage = ko.computed(function () {
                var arrayFilter = ko.utils.arrayFilter(model.files(), compare);
                return Math.ceil(arrayFilter.length / perPage) || 1;
            });
        });



        model.recordRemove = function (item) {
            if (confirm("Are you sure?")) {
                $.ajax({
                    dataType: "json",
                    type: "post",
                    url: options.urlDelete || "",
                    data: {
                        id: item.Id
                    },
                    success: function (data) {
                        if (data) {
                            model.files.remove(item);
                        }
                    }
                });
            }
        };

        model.authChange = function(item) {
            $.ajax({
                dataType: "json",
                type: "post",
                url: options.urlChangeAuth || "",
                data: {
                    id: item.Id,
                    authRequired: item.AuthRequired
                },
                success: function (data) {
                    if (data) {
                        jSuccess("<i class='fa fa-info-circle' style='color:#666;padding-right:8px'></i> Saved!</textarea>",
                        {
                            ShowOverlay: true,
                            autoHide: true,
                            clickOverlay: true,
                            onCompleted: function() {
                            }
                        });
                    }
                }
            });
        };

    })();
};